package com.epamtasks.animals;

public abstract class Animal implements Runnable, Comparable<Animal> {
    protected Color color;
    protected String name;
    protected int id;
    public void eat(){
        System.out.println("The "+ color.name().toLowerCase() + " " + this.getClass().getSimpleName().toLowerCase() + " (NAME: "
                + name + " ID: " + id + ") is eating anything");
    }

    public void sleep(){
        System.out.println("The "+ color.name().toLowerCase() + " " + this.getClass().getSimpleName().toLowerCase() + " (NAME: "
                + name + " ID: " + id + ") is sleeping");
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int compareTo(Animal o) {
        if (this.name.compareTo(o.name) > 0)
            return 1;
        else if (this.name.compareTo(o.name) < 0)
            return -1;
        else if(this.id > o.id)
            return 1;
        else if(this.id < o.id)
            return -1;
        return 0;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + id;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Animal other = (Animal) obj;
        if (color != other.color)
            return false;
        if (id != other.id)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
