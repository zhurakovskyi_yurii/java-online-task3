package com.epamtasks.animals;

public abstract class Bird extends Animal implements Runnable {
    public void fly(){
        System.out.println("The "+ color.name().toLowerCase() + " " + this.getClass().getSimpleName().toLowerCase()
                + " (NAME: " + name + " ID: " + id + ") is flying");
    }

    public void run() {
        try {
            Thread.sleep(600);
            fly();
            Thread.sleep(600);
            eat();
            Thread.sleep(600);
            sleep();
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
