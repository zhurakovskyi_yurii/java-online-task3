package com.epamtasks.animals;

public enum Color {
    BLACK, BROWN, WHITE, YELLOW, BLUE, ORANGE, GREEN, PINK, NAVY;
}
