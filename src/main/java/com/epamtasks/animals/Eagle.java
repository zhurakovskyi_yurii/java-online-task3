package com.epamtasks.animals;

public class Eagle extends Bird{
    public static int count;

    public Eagle() {
        id = 2000 + count;
        count++;

        int intColor = (int) (Math.random()*3);
        for (int i = 0; i < Color.values().length; i++) {
            if(Color.values()[i].ordinal() == intColor)
                color = Color.values()[i];
        }

        int intName = (int) (Math.random()*18);
        for (int i = 0; i < EagleName.values().length; i++) {
            if(EagleName.values()[i].ordinal() == intName)
                name = EagleName.values()[i].name();
        }
    }

    @Override
    public void eat() {
        System.out.println("The " + color.name().toLowerCase() + " eagle (NAME: " + name + " ID: " + id + ") is eating meat");
    }
}
