package com.epamtasks.animals;

public class Fish extends Animal implements Runnable {
    public static int count;

    public Fish() {
        id = 1000 + count;
        count++;

        int intColor = (int) (Math.random() * 9);
        for (int i = 0; i < Color.values().length; i++) {
            if (Color.values()[i].ordinal() == intColor)
                color = Color.values()[i];
        }
        int intName = (int) (Math.random() * 196);
        for (int i = 0; i < FishName.values().length; i++) {
            if (FishName.values()[i].ordinal() == intName)
                name = FishName.values()[i].name();
        }
    }

    public void swim() {
        System.out.println("The " + color.name().toLowerCase() + " fish (NAME: " + name + " ID: " + id +  ") is swimming");
    }

    public void run() {
        try {
            Thread.sleep(600);
            swim();
            Thread.sleep(600);
            eat();
            Thread.sleep(600);
            sleep();
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
