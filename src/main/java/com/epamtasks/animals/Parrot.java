package com.epamtasks.animals;

public class Parrot extends Bird {
    public static int count;

    public Parrot() {
        id = 3000 + count;
        count++;
        int intColor = (int) (Math.random()*9);
        for (int i = 0; i < Color.values().length; i++) {
            if(Color.values()[i].ordinal() == intColor)
                color = Color.values()[i];
        }
        int intName = (int) (Math.random()*80);
        for (int i = 0; i < ParrotName.values().length; i++) {
            if(ParrotName.values()[i].ordinal() == intName)
                name = ParrotName.values()[i].name();
        }
    }

    @Override
    public void eat() {
        System.out.println("The " + this.color.name().toLowerCase() + " parrot (NAME: " + name + " ID: " + id + ") is eating plants");
    }
}
