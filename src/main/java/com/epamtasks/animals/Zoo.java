package com.epamtasks.animals;

import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Zoo {
    private Set<Animal> animals;
    private ExecutorService executorService;
    private static Zoo instance;

    private Zoo() {
        animals = new TreeSet<Animal>();
        for (int i = 0; i < 15; i++) {
            animals.add(new Parrot());
        }

        for (int i = 0; i < 10; i++) {
            animals.add(new Eagle());
        }

        for (int i = 0; i < 18; i++) {
            animals.add(new Fish());
        }

        executorService = Executors.newScheduledThreadPool(25);
        System.out.println("My Zoo is opened!");
        for (Animal animal: animals) {
            executorService.execute(animal);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        executorService.shutdown();
    }

    public static Zoo getInstance() {
        if (instance == null) {
            instance = new Zoo();
        }
        return instance;
    }
}
