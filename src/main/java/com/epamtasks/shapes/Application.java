package com.epamtasks.shapes;


import java.awt.*;
import java.awt.Color;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import javax.swing.*;

class TriangleSh extends Path2D.Double {
    public TriangleSh(Point2D... points) {
        moveTo(points[0].getX(), points[0].getY());
        lineTo(points[1].getX(), points[1].getY());
        lineTo(points[2].getX(), points[2].getY());
        closePath();
    }
}

class Surface extends JPanel {

    ArrayList<com.epamtasks.shapes.Shape> shapes;
    public Surface(ArrayList<com.epamtasks.shapes.Shape> shapes) {
        this.shapes = shapes;
    }

    private void draw(Graphics graphics, ArrayList<com.epamtasks.shapes.Shape> shapes) {
        Graphics2D graphics2D = (Graphics2D) graphics;
        RenderingHints renderingHints = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        renderingHints.put(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHints(renderingHints);
        int coordChange = 0;
        for (Shape shape: shapes) {
            switch (shape.getColor()){
                case BLUE:
                    graphics2D.setPaint(Color.BLUE);
                    break;
                case GRAY:
                    graphics2D.setPaint(Color.GRAY);
                    break;
                case NAVY:
                    graphics2D.setPaint(new Color(0, 0, 128));
                    break;
                case PINK:
                    graphics2D.setPaint(Color.PINK);
                    break;
                case ORANGE:
                    graphics2D.setPaint(Color.ORANGE);
                    break;
                case YELLOW:
                    graphics2D.setPaint(Color.YELLOW);
                    break;
                case BROWN:
                    graphics2D.setPaint(new Color(139,69,19));
                    break;
                case GREEN:
                    graphics2D.setPaint(Color.GREEN);
                    break;
                case WHITE:
                    graphics2D.setPaint(Color.WHITE);
                    break;
                case PURPLE:
                    graphics2D.setPaint(new Color(128,0,128));
                    break;
                default:
                    graphics2D.setPaint(Color.BLACK);
                    break;
            }
            if(shape instanceof com.epamtasks.shapes.Rectangle){
                graphics2D.fillRect(shape.points.get(0).getX(), shape.points.get(0).getY()
                        ,((com.epamtasks.shapes.Rectangle) shape).getWidth(), ((Rectangle) shape).getHeight());
            } else if(shape instanceof Triangle){
                Point p1 = shape.getPoints().get(0);
                Point p2 = shape.getPoints().get(1);
                Point p3 = shape.getPoints().get(2);
                TriangleSh triangle = new TriangleSh(new Point2D.Double(p1.getX(), p1.getY()),
                        new Point2D.Double(p2.getX(), p2.getY()), new Point2D.Double(p3.getX(), p3.getY()));
                graphics2D.fill(triangle);
            } else if(shape instanceof Circle) {
                Point p = shape.getPoints().get(0);
                int radius = ((Circle) shape).getRadius();
                graphics2D.fillOval(p.getX(), p.getY(), radius * 2, radius * 2);
            }
            if(shape instanceof IArea){
                IArea iArea = (IArea) shape;
                graphics2D.drawString("Area of the "  +shape.getClass().getSimpleName() + " is " + iArea.area(), 20, 580 + coordChange);
                coordChange += 20;
            }

        }
        
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        draw(graphics, shapes);
    }
}


public class Application extends JFrame {
    ArrayList<com.epamtasks.shapes.Shape> shapeArrayList;

    public Application(){
        shapeArrayList = new ArrayList<com.epamtasks.shapes.Shape>();
        initGUI();
    }

    private void initGUI(){
       
        setTitle("Shapes");
        setSize(1080, 800);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        final ArrayList<com.epamtasks.shapes.Shape> shapes = new ArrayList<com.epamtasks.shapes.Shape>();
        int move = 0;
        int color = 0;
        for (int i = 0; i < 3; i++) {
            color = (int) (Math.random()*10);
            shapes.add(new com.epamtasks.shapes.Rectangle(com.epamtasks.shapes.Color.values()[color] , 10 + move, 5 , 160, 160));
            move += 200;
        }
        move = 0;
        for (int i = 0; i < 3; i++) {
            color = (int) (Math.random()*10);
            shapes.add(new Triangle(com.epamtasks.shapes.Color.values()[color], 160 + move, 250, 310 + move
                    , 500, 10 + move, 500));
            move += 160;
        }
        move = 0;
        for (int i = 0; i < 3; i++) {
            color = (int) (Math.random()*10);
            shapes.add(new Circle(com.epamtasks.shapes.Color.values()[color], 640 + move, 120 + move, 130));
            move += 30;
        }

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Application application = new Application();
                application.setVisible(true);
                Surface panel = new Surface(shapes);
                application.add(panel);
            }
        });
    }
}
