package com.epamtasks.shapes;

public class Circle extends Shape implements IArea {
    private int radius;
    public Circle(Color color, int x, int y, int radius) {
        super(color);
        this.radius = radius;
        points.add(new Point(x, y));
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public long area() {
        return (long) (Math.PI * Math.pow(radius, 2));
    }
}
