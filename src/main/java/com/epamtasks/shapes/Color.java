package com.epamtasks.shapes;

public enum Color {
    BLACK, BROWN, WHITE, YELLOW, BLUE, ORANGE, GREEN, PINK, NAVY, GRAY, PURPLE;
}

