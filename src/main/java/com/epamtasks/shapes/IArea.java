package com.epamtasks.shapes;

public interface IArea {
    long area();
}
