package com.epamtasks.shapes;

public class Rectangle extends Shape implements IArea {

    private int height;
    private int width;

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Rectangle(Color color, int x, int y, int height, int width) {
        super(color);
        this.height = height;
        this.width = width;
        points.add(new Point(x, y));
        points.add(new Point(x + width, y));
        points.add(new Point(x + width, y + height));
        points.add(new Point(x, y + height));
    }

    public long area() {
        return height * width;
    }
}
