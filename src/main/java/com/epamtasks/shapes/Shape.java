package com.epamtasks.shapes;

import java.util.*;

public abstract class Shape {
    protected Color color;
    protected String name;
    protected ArrayList<Point> points;

    public Shape(Color color) {
        this.color = color;
        String colorS = color.name().toLowerCase();
        colorS = colorS.replaceFirst(String.valueOf(colorS.toCharArray()[0])
                , String.valueOf(Character.toUpperCase(colorS.toCharArray()[0])));
        this.name = colorS + " " + this.getClass().getSimpleName();
        points = new ArrayList<Point>();
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Point> getPoints() {
        return points;
    }

    public static Set<Shape> getShapeByColor(String color, Set<Shape> shapes){
        if (shapes == null)
            return null;
        color = color.toUpperCase();
        Set<Shape> shapesTmp = new HashSet<Shape>();
        for ( Shape shape: shapes) {
            if(shape.getColor().name().equals(color))
                shapesTmp.add(shape);
        }
        return shapesTmp;
    }

    @Override
    public String toString() {
        return name + " " + Arrays.toString(points.toArray());
    }
}
