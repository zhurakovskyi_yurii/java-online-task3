package com.epamtasks.shapes;

public class Triangle extends Shape implements IArea {
    private long a;
    private long b;
    private long c;
    public Triangle(Color color, int x1, int y1, int x2, int y2, int x3, int y3) {
        super(color);
        a = (long) Math.sqrt(Math.pow(x2 - x1, 2)
                + Math.pow(y2 - y1, 2));
        b = (int) Math.sqrt(Math.pow(x3 - x2, 2)
                + Math.pow(y3 - y2, 2));
        c = (int) Math.sqrt(Math.pow(x3 - x1, 2)
                + Math.pow(y3 - y1, 2));
        if (isExists(a, b, c)) {
            points.add(new Point(x1, y1));
            points.add(new Point(x2, y2));
            points.add(new Point(x3, y3));
        }
    }

    public boolean isExists(long a, long b, long c){
        if(a + b > c && a + c > b && b + c > a)
            return true;
        return false;
    }

    public long area() {
        long p = perimeter();
        return (long) Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    public long perimeter(){
        return a + b + c;
    }
}
